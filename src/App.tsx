import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import React from 'react';
import { theme } from 'maroom-ui-kit'
import routes, { RoutAuthTypes } from './routes';
import { Route, Switch } from 'react-router';
import LoggedInRoute from './routes/LoggedInRoute';
import LoggedOutRoute from './routes/LoggedOutRoute';
import './index.css'

const appTheme = createMuiTheme({
  ...theme
})

function App() {
  return (
    <ThemeProvider theme={appTheme}>
      <Switch>
        {routes.map((route, index) => (
          route.auth === RoutAuthTypes.free ?
            <Route exact path={route.path} key={index}>
              {route.content}
            </Route> :
            route.auth === RoutAuthTypes.loggedIn ?
              <LoggedInRoute path={route.path} key={index}>
                {route.content}
              </LoggedInRoute> :
              <LoggedOutRoute path={route.path} key={index}>
                {route.content}
              </LoggedOutRoute>
        ))}
      </Switch>
    </ThemeProvider>
  );
}

export default App;
