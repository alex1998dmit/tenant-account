import React from "react";
import PropTypes from "prop-types";

const ChevronIcon = (props) => {
  const {
    width,
    height
  } = props;
  return (
    <svg width={width} height={height} viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.86193 0.195262L4 3.05719L1.13807 0.195262C0.877722 -0.0650874 0.455612 -0.0650874 0.195262 0.195262C-0.0650874 0.455612 -0.0650874 0.877722 0.195262 1.13807L3.5286 4.4714C3.78895 4.73175 4.21105 4.73175 4.4714 4.4714L7.80474 1.13807C8.06509 0.877722 8.06509 0.455612 7.80474 0.195262C7.54439 -0.0650874 7.12228 -0.0650874 6.86193 0.195262Z"
        fill="#8A94A6"
      />
    </svg>
  );
};

ChevronIcon.defaultProps = {
  width: 12,
  height: 5,
};

ChevronIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
};

export default ChevronIcon;
