import React from "react"

type IconProps = {
  width: number
  height: number
}

const DotsIcon = (props: IconProps) => {
  const {
    width,
    height
  } = props
  return (
    <svg width={width} height={height} viewBox="0 0 12 4" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M0 1.99996C0 2.73634 0.596954 3.33329 1.33333 3.33329C2.06971 3.33329 2.66667 2.73634 2.66667 1.99996C2.66667 1.26358 2.06971 0.666626 1.33333 0.666626C0.596954 0.666626 0 1.26358 0 1.99996ZM6 3.33329C5.26362 3.33329 4.66666 2.73634 4.66666 1.99996C4.66666 1.26358 5.26362 0.666626 6 0.666626C6.73638 0.666626 7.33333 1.26358 7.33333 1.99996C7.33333 2.73634 6.73638 3.33329 6 3.33329ZM10.6667 3.33329C9.93029 3.33329 9.33333 2.73634 9.33333 1.99996C9.33333 1.26358 9.93029 0.666626 10.6667 0.666626C11.403 0.666626 12 1.26358 12 1.99996C12 2.73634 11.403 3.33329 10.6667 3.33329Z" fill="#7F8285"/>
    </svg>
  )
}

DotsIcon.defaultProps = {
  width: 12,
  height: 4,
}

export default DotsIcon
