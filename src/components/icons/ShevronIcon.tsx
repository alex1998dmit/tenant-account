import * as React from "react"

type IconProps = {
  width: number
  height: number
}

const ShevronIcon = (props: IconProps) => {
  const {
    width,
    height
  } = props
  return (
    <svg width={width} height={height} viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M7.08573 6.00005L4.60306 8.48272C4.4357 8.65009 4.4357 8.92145 4.60306 9.08881C4.77043 9.25618 5.04179 9.25618 5.20916 9.08881L7.99487 6.3031C8.16224 6.13573 8.16224 5.86437 7.99487 5.69701L5.20916 2.91129C5.04179 2.74392 4.77043 2.74392 4.60306 2.91129C4.4357 3.07866 4.4357 3.35002 4.60306 3.51738L7.08573 6.00005Z" fill="#949494"/>
    </svg>
  )
}

ShevronIcon.defaultProps = {
  width: 12,
  height: 12,
}

export default ShevronIcon
