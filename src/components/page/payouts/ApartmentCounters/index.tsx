import { makeStyles } from '@material-ui/core'
import { observer } from 'mobx-react'
import * as React from 'react'
import useStores from '../../../../hooks/useStores'
import { TarrifsParams } from '../../../../types/api/Tarrifs'
import ApartmentAccordion from '../../../parts/ApartmentAccordion'
import Counters from '../Counters'

const useStyles = makeStyles({
  wrapper: {
    padding: '48px 0'
  }
})

const ApartmentCounters = () => {
  const { tarrifsStore: { fetchItem, item } } = useStores()
  const classes = useStyles()
  React.useEffect(() => {
    const as = async () => {
      await fetchItem()
    }

    as()
  }, [])

  return (
    <div className={classes.wrapper}>
      <ApartmentAccordion
        address={'Ул. Русаковская, 4с1'}
        content={
          <Counters />
        }
      />
    </div>
  )
}

export default observer(ApartmentCounters)
