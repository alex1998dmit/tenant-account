import { Grid, Icon, makeStyles } from '@material-ui/core'
import { Caption, CaptionSizes, CaptionWeights, Input, ThemeInterface } from 'maroom-ui-kit'
import * as React from 'react'
import { useForm } from 'react-hook-form'
import ChevronIcon from '../../../../icons/Chevron'

type Props = {
  label: string
  tariff: number
  unit?: string
}

const useStyles = makeStyles((theme: ThemeInterface) => ({
  root: {
    alignItems: 'center',
    borderBottom: `1px solid ${theme.palette.secondary.light3}`,
    minHeight: '96px'
  },
  slash: {
    padding: '0 20px',
    height: 'unset',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 'unset',
    '& svg': {
      transform: 'rotate(270deg)'
    }
  },
  price: {
    width: '25%'
  }
}))

const CounterRow = (props: Props) => {
  const { label, tariff, unit } = props
  const classes = useStyles()
  const form = useForm()
  return (
    <Grid container item xs={12} className={classes.root}>
      <Grid item xs={6}>
        <Caption
          size={CaptionSizes.s}
          weight={CaptionWeights.medium}
        >
          {label}
        </Caption>
      </Grid>
      <Grid
        item
        xs={6}
        container
        alignItems='center'
        justify='flex-end'
      >
        <Input
          type={'number'}
          name={'editing'}
          label={unit}
          inputRef={form.register}
          defaultValue={1333}
          fullWidth={false}
        />
        <div className={classes.slash}>
          <ChevronIcon width={12} />
        </div>
        <div className={classes.price}>
          <Caption>
            {(tariff * (form.watch('editing') || 1333))} ₽
          </Caption>
        </div>
      </Grid>
    </Grid>
  )
}

export default CounterRow
