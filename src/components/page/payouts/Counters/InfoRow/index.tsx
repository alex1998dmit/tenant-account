import { Grid, makeStyles } from '@material-ui/core'
import { Caption, CaptionSizes, CaptionWeights, ThemeInterface } from 'maroom-ui-kit'
import * as React from 'react'

type Props = {
  label: string
  value: string
}

const useStyles = makeStyles((theme: ThemeInterface) => ({
  root: {
    alignItems: 'center',
    borderBottom: `1px solid ${theme.palette.secondary.light3}`,
    minHeight: '96px'
  },
  slash: {
    padding: '0 20px',
    height: 'unset',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 'unset',
    '& svg': {
      transform: 'rotate(270deg)'
    }
  },
  price: {
    width: '25%'
  }
}))

const InfoRow = (props: Props) => {
  const { label, value } = props
  const classes = useStyles()
  return (
    <Grid container item xs={12} className={classes.root}>
      <Grid item xs={6}>
        <Caption
          size={CaptionSizes.s}
          weight={CaptionWeights.medium}
        >
          {label}
        </Caption>
      </Grid>
      <Grid
        item
        xs={6}
        container
        alignItems='center'
        justify='flex-end'
      >
        <div className={classes.price}>
          <Caption>
            {value}
          </Caption>
        </div>
      </Grid>
    </Grid>
  )
}

export default InfoRow
