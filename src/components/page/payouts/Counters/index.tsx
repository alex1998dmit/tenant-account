import classes from '*.module.css'
import { Grid, makeStyles } from '@material-ui/core'
import { Button, ButtonThemes, Caption, CaptionWeights, Heading, HeadingSizes, HeadingWeights } from 'maroom-ui-kit'
import { observer } from 'mobx-react'
import * as React from 'react'
import useStores from '../../../../hooks/useStores'
import { TarrifParams, TarrifsParams } from '../../../../types/api/Tarrifs'
import TarrifRow from './CounterRow'
import InfoRow from './InfoRow'

const useStyles = makeStyles({
  footer: {
    marginTop: '8px',
  }
})

const Counters = () => {
  const classes = useStyles()
  return (
    <Grid container spacing={2}>    
      <TarrifRow
        label={'Счетчик электроэнергии Т3'}
        tariff={5.67}
        unit={'кВт'}
      />
      <TarrifRow
        label={'Счетчик электроэнергии Т2'}
        tariff={5.67}
        unit={'кВт'}
      />
      <TarrifRow
        label={'Счетчик электроэнергии Т1'}
        tariff={5.67}
        unit={'кВт'}
      />
      <TarrifRow
        label={'Счетчик ГВС'}
        tariff={5.67}
        unit={'Куб'}
      />
      <TarrifRow
        label={'Водоотведение'}
        tariff={5.67}
        unit={'Куб'}
      />
      <InfoRow
        label={'Аренда жилья'}
        value={'+38 0000 ₽'}
      />
      <Grid item xs={12} container alignContent={'center'} className={classes.footer}>
        <Grid item xs={6} container alignContent={'center'}>
          <Button
            theme={ButtonThemes.brandPrimary}
            fullWidth={false}
          >
             Скачать квитанцию
          </Button>
        </Grid>
        <Grid item xs={6} container spacing={1}>
          <Grid item xs={12} container justify='flex-end'>
            <Heading
              size={HeadingSizes.h2}
              weight={HeadingWeights.bold}
            >
              {'360000'.toLocaleString()} ₽
            </Heading>
          </Grid>
          <Grid item xs={12} container justify='flex-end'>
            <Caption weight={CaptionWeights.medium}>
              Итого на июль
            </Caption>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default observer(Counters)
