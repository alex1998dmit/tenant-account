import { Grid, makeStyles } from '@material-ui/core'
import * as React from 'react'
import { useForm } from 'react-hook-form'
import * as yup from "yup"
import {yupResolver} from '@hookform/resolvers/yup';
import SignInForm from '../../../forms/SignIn';
import { SignInParams } from '../../../types/store/Auth';

const useStyles = makeStyles({
  root: {
    background: 'white',
    borderRadius: '6px',
    boxShadow: '0px 0px 1px rgba(10, 31, 68, 0.1), 0px 26px 26px rgba(10, 31, 68, 0.12)',
    padding: '120px 64px'
  }
})

const LoginSchema = yup.object().shape({
  username: yup.string()
    .required("Заполните поле"),
  password: yup.string().required("Введите пароль")
})

type Inputs = {
  username: string
  password: string
}

const SignInModal = () => {
  const classes = useStyles()
  const form = useForm<Inputs>({
    resolver: yupResolver(LoginSchema)
  })
  return (
    <div className={classes.root}>
      <SignInForm<SignInParams>
        form={form}
        formHandler={(data) => console.log(data)}  
      />
    </div>
  )
}

export default SignInModal
