import { makeStyles } from '@material-ui/core'
import { Caption, CaptionSizes, CaptionWeights } from 'maroom-ui-kit'
import * as React from 'react'
import { Accordion, AccordionDetails, AccordionSummary } from '../../ui/Accordion'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const useStyles = makeStyles({
  summaryWrapper: {
    display: 'flex',
    alignItems: 'center'
  },
  imgApartment: {
    width: '56px',
    height: '56px',
    objectFit: 'cover',
    borderRadius: '6px',
    marginRight: '24px'
  }
})

type Props = {
  address: string
  image_url?: string
  content: React.ReactNode
}

const ApartmentAccordion = (props: Props) => {
  const classes = useStyles()
  const { address, image_url, content } = props
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
      >
        <div className={classes.summaryWrapper}>
          <img
            className={classes.imgApartment}
            src='https://media.comicbook.com/2018/10/lord-of-the-rings-eye-of-sauron-tower-halloween-petition-1141484-1280x0.jpeg'
          />
          <Caption size={CaptionSizes.s} weight={CaptionWeights.medium}>
            {address}
          </Caption>
        </div>      
      </AccordionSummary>
      <AccordionDetails>
        {content}
      </AccordionDetails>
    </Accordion>
  )
}

export default ApartmentAccordion
