import * as React from 'react'
import { AppBar, Icon, makeStyles, Toolbar, Container } from '@material-ui/core'
import { Button, ButtonThemes, CircleButton, CircleButtonThemes, ThemeInterface } from 'maroom-ui-kit'
import MenuPoint from './includes/MenuPoint'
import AvatarDropdown from '../../ui/AvatarDropdown'
import MaroomMiniIcon from '../../icons/MaroomMini'

const useStyles = makeStyles((theme: ThemeInterface) => ({
  wrapper: {
    // borderBottom: `1px solid ${theme.palette.secondary.light3}`
    border: 'none'
  },
  logo: {
    width: '2em'
  },
  root: {
    background: 'white',
    border: 'none',
    boxShadow: 'none',
  },
  toolBarRoot: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '56px',
    padding: 0
  },
  leftMenu: {
    display: 'flex',
    width: '100%',
    alignItems: 'center'
  },
  rightMenu: {

  },
  leftMenuItems: {
    display: 'flex',
    width: '100%',
    alignItems: 'center'
  },
}))

const AuthHeader = () => {
  const classes = useStyles()
  return (
    <div className={classes.wrapper}>
      <AppBar
        position='static'
        classes={{
          root: classes.root
        }}
      >
        <Container>
          <Toolbar classes={{ root: classes.toolBarRoot }}>
            <div className={classes.leftMenu}>
              <Icon classes={{
                root: classes.logo
              }}>
                <MaroomMiniIcon />
              </Icon>
              <div className={classes.leftMenuItems}>
                <MenuPoint>
                  Главная
                </MenuPoint>
                <MenuPoint>
                  Оплата
                </MenuPoint>
                <MenuPoint>
                  Календарь
                </MenuPoint>
                <MenuPoint>
                  Документы
                </MenuPoint>
                <MenuPoint>
                  Сожители
                </MenuPoint>
              </div>
            </div>
            <div className={classes.rightMenu}>
              <AvatarDropdown
                user={{
                  name: 'Alexander'
                }}
              />
            </div>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  )
}

export default AuthHeader
