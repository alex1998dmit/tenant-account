import * as React from 'react'
import { AppBar, Icon, makeStyles, Toolbar, Container } from '@material-ui/core'
import { Button, ButtonThemes, CircleButton, CircleButtonThemes, ThemeInterface } from 'maroom-ui-kit'
import MenuPoint from './includes/MenuPoint'

const useStyles = makeStyles((theme: ThemeInterface) => ({
  wrapper: {
    borderBottom: `1px solid ${theme.palette.secondary.light3}`
  },
  root: {
    background: 'white',
    border: 'none',
    boxShadow: 'none',
  },
  toolBarRoot: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '56px',
    padding: 0
  },
  leftMenu: {

  },
  rightMenu: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  menuItems: {
    paddingRight: '24px',
    display: 'flex'
  }
}))

const UnAuthHeader = () => {
  const classes = useStyles()
  return (
    <div className={classes.wrapper}>
        <AppBar
          position='static'
          classes={{
            root: classes.root
          }}
        >
          <Container>
          <Toolbar classes={{ root: classes.toolBarRoot }}>
            <div className={classes.leftMenu}>
              <CircleButton
                theme={CircleButtonThemes.white}
              >
                -
              </CircleButton>
            </div>
            <div className={classes.rightMenu}>
              <div className={classes.menuItems}>
                <MenuPoint>
                  Главная
                </MenuPoint>
                <MenuPoint>
                  Снять
                </MenuPoint>
                <MenuPoint>
                  О сервисе
                </MenuPoint>
                <MenuPoint>
                  Блог
                </MenuPoint>
              </div>          
              <Button
                theme={ButtonThemes.brandPrimary}
                fullWidth={false}
              >
                Войти
              </Button>
            </div>
          </Toolbar>
          </Container>
      </AppBar>
    </div>
  )
}

export default UnAuthHeader
