import { makeStyles } from '@material-ui/core'
import { Caption, CaptionWeights } from 'maroom-ui-kit'
import * as React from 'react'

type Props = {
  active?: boolean,
  children?: string
}

const useStyles = makeStyles({
  root: {
    padding: '0 12px',
    '&:hover': {
      cursor: 'pointer'
    }
  }
})

const MenuPoint = (props: Props) => {
  const { children, active } = props
  const classes = useStyles()
  return (
    <div
      className={classes.root}
    >
      <Caption weight={CaptionWeights.medium}>
        {children}
      </Caption>
    </div>
  )
}

MenuPoint.defaultProps = {
  active: false
}

export default MenuPoint
