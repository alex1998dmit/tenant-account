import { makeStyles } from '@material-ui/core'
import classNames from 'classnames'
import { ThemeInterface } from 'maroom-ui-kit'
import { observer } from 'mobx-react'
import * as React from 'react'
import useStores from '../../../hooks/useStores'
import AuthHeader from '../header/AuthHeader'
import UnAuthHeader from '../header/UnAuthHeader'

type Props = {
  children: React.ReactNode,
  wrapperContentClassName?: string
}

const useStyles = makeStyles((theme: ThemeInterface) => ({
  wrapper: {
    background: theme.palette.secondary.light3,
    minHeight: 'calc(100vh - 56px)'
  }
}))

const Page = (props: Props) => {
  const { appStore: { isAuthed } } = useStores()
  const { children, wrapperContentClassName } = props
  const classes = useStyles()
  return (
    <div>
      <div>
        {isAuthed && (<AuthHeader />)}
        {!isAuthed && (<UnAuthHeader />)}
      </div>
      <div
        className={
          classNames(
            classes.wrapper,
            wrapperContentClassName
            )}
      >
        {children}
      </div>
    </div>
  )
}

export default observer(Page)
