import {
  Accordion as MUIAccordion,
  AccordionSummary as MUIAccordionSummary,
  AccordionDetails as MUIAccordionDetails,
  withStyles
} from '@material-ui/core'
import { ThemeInterface } from 'maroom-ui-kit'
import * as React from 'react'

const Accordion = withStyles(() => ({
  root: {
    boxShadow: 'none',
    border: `1px solid #E1E4E8`
  }
}))(MUIAccordion)

const AccordionSummary = withStyles({
})(MUIAccordionSummary)

const AccordionDetails = withStyles({
  root: {
    borderTop: `1px solid #E1E4E8`
  }
})(MUIAccordionDetails)

export {
  Accordion,
  AccordionSummary,
  AccordionDetails
}