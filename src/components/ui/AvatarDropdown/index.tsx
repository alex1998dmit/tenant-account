import { makeStyles } from '@material-ui/core'
import { Caption, CaptionWeights } from 'maroom-ui-kit'
import * as React from 'react'
import UndefinedAvatar from '../UndefinedAvatar'

type UserInfo = {
  name: string
  avatar?: string
}

type Props = {
  user: UserInfo
}

const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center'
  },
  label: {
    paddingLeft: '15px'
  }
})

const AvatarDropdown = (props: Props) => {
  const { user } = props
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <UndefinedAvatar label={'Alexander Dmitriev'} />
      <Caption weight={CaptionWeights.medium} className={classes.label}>
        Александр
      </Caption>
    </div>
  )
}

export default AvatarDropdown
