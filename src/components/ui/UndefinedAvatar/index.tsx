import * as React from "react"
import {makeStyles} from "@material-ui/styles";
import {ThemeInterface} from "maroom-ui-kit";

type styleProps = {
  size?: UndefinedAvatarSizes
}

const useStyles = makeStyles((theme: ThemeInterface) => ({
  root: (props: styleProps) => ({
    width: props.size === UndefinedAvatarSizes.sm ? '32px' : '84px',
    height: props.size === UndefinedAvatarSizes.sm ? '32px' : '84px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.primary.background,
    color: theme.palette.primary.main,
    borderRadius: '66px',
    fontSize: props.size === UndefinedAvatarSizes.sm ? '12px' : '32px',
    fontWeight: 600,
    lineHeight: '14px'
  })
}))

export enum UndefinedAvatarSizes {
  sm= 'sm',
  lg = 'lg'
}

type Props = {
  label: string | null | undefined,
  size?: UndefinedAvatarSizes
}

const UndefinedAvatar = (props: Props) => {
  const classes = useStyles({ size: props.size })
  const { label } = props
  // const formattedLabel = label.split(' ').map((el: string) => el[0].toUpperCase()).join('')
  if (!label) {
    return <div></div>
  }
  const formattedLabel = label.split(' ').map((el: string) => el[0]).slice(0, 3).join('')
  return (
    <div className={classes.root}>
      {formattedLabel}
    </div>
  )
}

UndefinedAvatar.defaultProps = {
  size: UndefinedAvatarSizes.sm
}

export default UndefinedAvatar
