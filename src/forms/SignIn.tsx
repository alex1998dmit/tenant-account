import { Grid } from '@material-ui/core'
import { Button, Caption, Heading, HeadingSizes, HeadingWeights, Input } from 'maroom-ui-kit'
import * as React from 'react'

type Props<T> = {
  form: any,
  formHandler: (data: T) => void
}

function SignInForm<T>(props: React.PropsWithChildren<Props<T>>) {
  const { form, formHandler } = props
  return (
    <form onSubmit={form.handleSubmit(formHandler)}>
      <Grid container spacing={3}>
        <Grid item xs={12} container justify='center'>
          <Heading
            size={HeadingSizes.h2}
            weight={HeadingWeights.bold}
          >
            Войти в Maroom
          </Heading>
        </Grid>
        <Grid item xs={12}>
          <Input
            inputRef={form.register}
            label={'username'}
            name={'username'}
            error={!!form.errors.username}
            errorMsg={form.errors.username ? form.errors.username.message : ''}
          />
        </Grid>
        <Grid item xs={12}>
          <Input
            inputRef={form.register}
            label={'password'}
            name={'password'}
            type={'password'}
            error={!!form.errors.password}
            errorMsg={form.errors.password ? form.errors.password.message : ''}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            type={'submit'}
          >
            Войти
          </Button>
        </Grid>
      </Grid>
    </form>
  )
}

export default SignInForm
