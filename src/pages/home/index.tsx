import { Container, makeStyles } from '@material-ui/core'
import { Heading, HeadingSizes, HeadingWeights, ThemeInterface } from 'maroom-ui-kit'
import * as React from 'react'
import ApartmentCounters from '../../components/page/payouts/ApartmentCounters'
import Page from '../../components/parts/page/Page'

const useStyles = makeStyles((theme: ThemeInterface) => ({
  titleWrapper: {
    padding: '40px 0',
    background: 'white'
  },
  contentWrapper: {

  }
}))

const HomePage = () => {
  const classes = useStyles()
  return (
    <Page>
      <div className={classes.titleWrapper}>
        <Container>
          <Heading size={HeadingSizes.h1} weight={HeadingWeights.bold}>
            Оплата
          </Heading>
        </Container>
      </div>
      <div className={classes.contentWrapper}>
        <Container>
          <ApartmentCounters />
        </Container>
      </div>
    </Page>
  )
}

export default HomePage
