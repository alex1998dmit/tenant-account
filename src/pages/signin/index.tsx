import { Container, Grid, makeStyles } from '@material-ui/core'
import * as React from 'react'
import SignInModal from '../../components/page/signin/modal'
import Page from '../../components/parts/page/Page'

const useStyles = makeStyles({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

const SignInPage = () => {
  const classes = useStyles()
  return (
    <Page
      wrapperContentClassName={classes.wrapper}
    >
      <Container>
        <Grid
          container
          justify='center'
          alignItems={'center'}
        >
          <Grid item xs={6}>
            <SignInModal />
          </Grid>
        </Grid>  
      </Container>      
    </Page>
  )
}

export default SignInPage
