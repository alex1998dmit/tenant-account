import * as React from 'react'
import useStores from '../hooks/useStores'
import { Redirect, Route } from 'react-router-dom'
import { observer } from 'mobx-react'

type Props = {
  children: React.ReactNode
  path: string
}

const LoggedInRoute = (props: Props) => {
  const { children, ...rest } = props
  const { appStore } = useStores()
  if (!appStore.isAuthed) {
    return (
      <Route exact {...rest}>
        {props.children}
      </Route>
    )
  }
  return <Redirect to={'/'} />
}

export default observer(LoggedInRoute)
