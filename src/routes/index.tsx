import React from "react"
import HomePage from "../pages/home"
import MainPage from "../pages/main"
import PayoutsPage from "../pages/payouts"
import SignInPage from "../pages/signin"

export enum RoutAuthTypes {
  loggedIn = 'logged_in',
  loggedOut = 'logged_out',
  free = 'free'
}

interface RouteInterface {
  path: string
  exact: boolean
  content: React.ReactNode
  auth: RoutAuthTypes
}

const routes: Array<RouteInterface> = [
  {
    path: '/login',
    exact: true,
    content: <SignInPage />,
    auth: RoutAuthTypes.loggedOut
  },
  {
    path: '/home',
    exact: true,
    content: <HomePage />,
    auth: RoutAuthTypes.loggedIn
  },
  {
    path: '/',
    exact: true,
    content: <MainPage />,
    auth: RoutAuthTypes.loggedIn
  },
  {
    path: '/payouts',
    exact: true,
    content: <PayoutsPage />,
    auth: RoutAuthTypes.loggedIn
  },
]

export default routes
