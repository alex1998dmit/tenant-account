import { makeAutoObservable } from "mobx"
import { RootStoreInterface } from "../types/store/Root"

class ApartmentStore {
  public items: any
  public rootStore: RootStoreInterface
  
  constructor(rootStore: RootStoreInterface) {
    makeAutoObservable(this.items, { rootStore: false })
    this.rootStore = rootStore
  }

  
}

export default ApartmentStore
