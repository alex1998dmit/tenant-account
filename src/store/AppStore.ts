import { RootStoreInterface } from "../types/store/Root";

class AppStore {
  public rootStore: RootStoreInterface

  constructor(rootStore: RootStoreInterface) {
    this.rootStore = rootStore
  }

  public get isAuthed() {
    return true
    
  }
}

export default AppStore
