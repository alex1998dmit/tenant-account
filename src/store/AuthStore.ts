import { AuthStoreInteface, SignInParams } from "../types/store/Auth";
import { RootStoreInterface } from "../types/store/Root";

class AuthStore implements AuthStoreInteface {
  public rootStore: RootStoreInterface
  public userInfo: null | {}
  
  constructor(rootStore: RootStoreInterface) {
    this.rootStore = rootStore
    this.userInfo = this.rootStore.appStore.isAuthed ?
      localStorage.getItem('user_info') :
      null
  }

  public async signin(data: SignInParams) {
    if (data.username === 'admin' && data.password === 'admin') {
      localStorage.setItem('user', JSON.stringify({
        id: 1,
        username: 'admin',
        apartment_id: 3,
      }))
    }
  }
}

export default AuthStore
