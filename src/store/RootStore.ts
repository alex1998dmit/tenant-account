import { AppStoreInterface } from "../types/store/App"
import { TarrifsStoreInterface } from "../types/store/Tarrifs"
import AppStore from "./AppStore"
import TarrifsStore from "./TarrifsStore"

class RootStore {
  public appStore: AppStoreInterface
  public tarrifsStore: TarrifsStoreInterface

  constructor() {
    this.appStore = new AppStore(this)
    this.tarrifsStore = new TarrifsStore(this)
  }
}

export default RootStore
