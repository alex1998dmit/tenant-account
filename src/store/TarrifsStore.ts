import { makeAutoObservable, runInAction } from 'mobx'
import * as React from 'react'
import { number } from 'yup/lib/locale'
import { TarrifsParams } from '../types/api/Tarrifs'
import { RootStoreInterface } from '../types/store/Root'
import { TarrifsStoreInterface } from '../types/store/Tarrifs'

class TarrifsStore implements TarrifsStoreInterface {
  public rootStore: RootStoreInterface
  public item: TarrifsParams | null

  constructor(rootStore: RootStoreInterface) {
    makeAutoObservable(this, { rootStore: false })
    this.rootStore = rootStore
    this.item = null
  }

  public fetchItem = async () => {
    runInAction(() => {
      this.item = {
        id: 1,
        electricty: {
          cost: 5.29,
          name: 'Электроэнергия',
          unit: 'кВт/час'
        },
        cold_water: {
          cost: 42.3,
          name: 'Холодное водоотведение',
          unit: 'Куб'
        },
        hot_water: {
          cost: 42.3,
          name: 'Холодное водоотведение',
          unit: 'Куб'
        },
        drainage: {
          cost: 205.5,
          name: 'Горячее водоотведение',
          unit: 'Куб'
        }
      }
    })
  }
}

export default TarrifsStore
