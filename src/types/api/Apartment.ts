export type Apartment = {
  id: number,
  address: string,
  tarrif_id: number
}