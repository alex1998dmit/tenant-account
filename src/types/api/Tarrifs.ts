export type TarrifParams = {
  cost: number
  name: string
  unit: string
}

export type TarrifsParams = {
  id: number
  electricty: TarrifParams,
  cold_water: TarrifParams,
  hot_water: TarrifParams,
  drainage: TarrifParams
}
