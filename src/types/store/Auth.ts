import { RootStoreInterface } from "./Root";

export type SignInParams = {
  username: string
  password: string
}

export interface AuthStoreInteface {
  rootStore: RootStoreInterface
  signin: (data: SignInParams) => void
}
