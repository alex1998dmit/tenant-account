import { AppStoreInterface } from "./App";

export interface RootStoreInterface {
  appStore: AppStoreInterface
}