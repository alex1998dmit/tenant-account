import { TarrifsParams } from "../api/Tarrifs";
import { RootStoreInterface } from "./Root";

export interface TarrifsStoreInterface {
  rootStore: RootStoreInterface
  item: TarrifsParams | null
  fetchItem: () => void
}
